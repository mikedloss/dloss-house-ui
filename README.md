# dloss-house 🏠

This is the app for the dloss house. We have cool things like games and movies, but we also like to let our house guests find other useful information here.

Written in React and Node, with help from create-react-app.