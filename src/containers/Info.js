import React, {Component} from 'react';
import { GoBackButton } from '../components/shared';

class Info extends Component {
  render() {
    return (
      <div>
        <GoBackButton route="/" />
        <h1>Hey!</h1>
        <p>
          This site has a list of our games and movies in case you wanted to entertain
          yourself. Hopefully you find it useful, I tried to make it useful. It'd be cool
          if you found this site useful. If you found it useful, let me (Mike) or Devanne
          know.
        </p>
        <p>
          If you need help with anything, you can let
          <a href="tel:+17244486183">Mike</a>
          or
          <a href="tel:+17249803173">Devanne</a>
          know.
        </p>
      </div>
    );
  }
}

export default Info;