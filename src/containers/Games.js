import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { FaPencil, FaTrash } from 'react-icons/lib/fa';

import { getAllGames } from '../API';
import { GoBackButton } from '../components/shared';

const TableStyle = styled.table`
  width: 100%;
  border-collapse: collapse;
  thead, tbody {
    tr { border-bottom: 1px solid #ddd; }
  }
  td, th {
    text-align: left;
    padding: 10px;
    margin: 0 auto;
    white-space: pre-wrap;
  }
`

class Games extends Component {

  state = {
    games: [],
    user: {}
  }

  componentDidMount = async () => {
    // get all the games
    let response = await getAllGames();
    if (response.status === 200) {
      let games = response.games.sort((a,b) => (a.game > b.game) ? 1 : ((b.game > a.game) ? -1 : 0)); 
      this.setState({ games });
    }
  }

  render() {
    let allGames = this.state.games.map(game => {
      return (
        <tr key={game._id}>
          <td>
            <Link to={`/games/${game._id}`}>
              {game.game}
            </Link>
          </td>
          <td>{game.players}</td>
          <td>{game.playTime}</td>
          <td>{game.difficulty}</td>
          <td>{game.type}</td>
          { this.props.userInfo.isAdmin && 
            <td><FaPencil style={{color: "#004D74"}} /></td>
          }
          { this.props.userInfo.isAdmin && 
            <td><FaTrash style={{color: "#9F81A9"}} /></td>
          }
        </tr>
      )
    });

    return (
      <div>
        <GoBackButton route="/" />
        <TableStyle>
          <thead>
            <tr>
              <th>Title</th>
              <th>Players</th>
              <th>Play Time</th>
              <th>Difficulty</th>
              <th>Type</th>
            </tr>
          </thead>
          <tbody>
            {allGames}
          </tbody>
        </TableStyle>
      </div>
    );
  }
}

export default Games;