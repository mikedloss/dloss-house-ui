import React, { Component } from 'react'
import styled from 'styled-components';
import { GoBackButton } from '../components/shared';
import { getGame } from '../API';

const GameInfo = styled.div`
  p {
    margin: 5px 0;
  }
`

class GameProfile extends Component {

  state = {
    loading: true,
    game: null,
    message: ''
  }

  componentDidMount = async () => {
    let gameId = this.props.match.params.id;
    let response = await getGame(gameId);
    if (response.status === 200) {
      this.setState({
        loading: false,
        game: response.game
      });
    } else {
      this.setState({
        loading: false,
        message: 'Error retrieving that game - it probably doesn\'t exist.'
      });
    }
  }

  render() {
    return (
      <div>
        <GoBackButton route="/games" />
        {this.state.message !== '' && <div>{this.state.message}</div>}
        {!this.state.loading && this.state.message === '' &&
          <GameInfo>
            <h1>{this.state.game.game}</h1>
            <p><strong>Difficulty:</strong> {this.state.game.difficulty} / 5</p>
            <p><strong>Player Count:</strong> {this.state.game.players}</p>
            <p><strong>Play Time:</strong> {this.state.game.playTime}</p>
            <p><strong>Type:</strong> {this.state.game.type}</p>
            <p><strong>Category:</strong> {this.state.game.category}</p>
          </GameInfo>
        }
      </div>
    )
  }
};

export default GameProfile;