import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { Notice } from '../components/shared';

const HeaderStyle = styled.div`
  h1 { 
    text-align: center;
    margin: 7px;
    a {
      font-size: 32px;
    }
  }
  border-bottom: 2px solid #E5F6FF;
  padding-bottom: 10px;
  margin-bottom: 20px;
`

const AccountRowStyle = styled.div`
  text-align: center;
  a,span {
    padding: 0 10px;
  }
`

class Header extends Component {
  componentDidMount = () => {}

  render() {
    const AccountOptions = () => {
      if (this.props.accountInfo.isLoggedIn) {
        return (
          <div>
            <span>welcome, {this.props.accountInfo.username}!</span>
            <Link to="/">view profile</Link>
            <Link to="/" onClick={this.props.logout}>log out</Link>
            {this.props.accountInfo.isAdmin && 
              <Link to="/admin">admin</Link>
            }
          </div>
        )
      } else {
        return (
          <div>
            <Link to="/login">sign in</Link>
            <Link to="/register">register</Link>
          </div>
        )
      }
    }

    return (
      <div>
        <HeaderStyle>
          <h1>
            <Link to="/">
              dloss <span role="img" aria-label="house">🏠</span>
            </Link>
          </h1>
          <AccountRowStyle>
            <AccountOptions />
          </AccountRowStyle>
        </HeaderStyle>
        <Notice />
      </div>
    );
  }
}

export default Header;