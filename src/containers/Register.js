import React, {Component} from 'react';
import styled from 'styled-components';

import {Notice} from '../components/shared';

const RegisterFormStyle = styled.form `
  margin-top: 10op
  width: 100%;

  label {
    h3 {
      margin-bottom: 10px;
    }
  }

  input {
    width: 100%;
    padding: 10px 0;
    background-color: #fff;
    border-radius: 3px;
    transition: all 0.1s ease-in-out;

    &[type="text"],
    &[type="password"] {
      border: 2px solid rgba(35, 138, 177, 0.25);
      &:focus {
        border-color: rgba(35, 138, 177, 1);
      }
    }

    &[type="submit"] {
      margin-top: 40px;
      border: 2px solid rgba(219, 112, 147, 0.2);
      &:hover {
        background-color: #DB7093;
        border-color: #DB7093;
        color: #fff;
      }
    }
  }

  small {
    font-style: italic;
    font-size: 12px;
    &.hint {
      display: block;
    }
    &.error {
      color: #C44646;
    }
  }
  
`

const initialState = {
  username: "",
  password: "",
  passwordConfirm: "",
  usernameError: "",
  passwordError: "",
  errors: []
}

class Register extends Component {

  state = initialState

  handleInputChange = (e) => {
    let name = e.target.name;
    this.setState({ [name]: e.target.value });
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    console.log(this.state);
    console.log(this.props);
    
    if (this.validateUsername(this.state.username) && this.validatePassword(this.state.password, this.state.passwordConfirm)) {
      this.setState(initialState);
      let response = await this.props.register(this.state.username, this.state.password);
      if (response.auth) {
        this.props.history.push('/');
      } else {
        this._handleError([response.message]);
      }
    } else {
      let errors = [];
      if (!this.validateUsername(this.state.username)) { 
        errors.push("Username does not meet requirements");
      }
      
      if (!this.validatePassword(this.state.password, this.state.passwordConfirm)) {
        errors.push("Passwords do not match");
      }

      this._handleError(errors);
    }
  }

  validateUsername = (username) => {
    let regex = /^[a-zA-Z]([._-]?[a-zA-Z0-9]+){2,}/g;
    return regex.test(username);
  }

  validatePassword = (password, passwordConfirm) => {
    return password === passwordConfirm;
  }

  componentWillMount = () => {
    if (localStorage.getItem('dloss_auth_token') != null) {
      console.log('already have an auth token!');
      this.props.history.push('/');
    }
  }  

  _handleError = (errors) => this.setState({ errors })

  render() {
    return (
      <div>
        <h1>Register</h1>
        <p>An account lets you save favorite games and movies, and see some extra stuff
          if you're housesitting.</p>
        { this.state.errors.length > 0 &&
          <Notice 
            description="Some errors found with registration"
            errors={this.state.errors}
          />
        }
        <RegisterFormStyle>
          <label>
            <h3>Username</h3>
            {this.state.usernameError !== "" &&
              <small className="error">{this.state.usernameError}</small>
            }
            <input type="text" name="username" onChange={this.handleInputChange} maxLength="20" required />
            <small className="hint">3 - 20 characters long, can have letters numbers - _</small>
          </label>
          <label>
            <h3>Password</h3>
            {this.state.passwordError !== "" &&
              <small className="error">{this.state.passwordError}</small>
            }
            <input type="password" name="password" onChange={this.handleInputChange} required />
          </label>
          <label>
            <h3>Confirm password</h3>
            <input type="password" name="passwordConfirm" onChange={this.handleInputChange} required />
          </label>

          <input type="submit" value="Submit" onClick={this.handleSubmit}/>
        </RegisterFormStyle>
      </div>
    );
  }
}

export default Register;