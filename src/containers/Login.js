import React, { Component } from 'react';
import styled from 'styled-components';

const LoginFormStyle = styled.form`
  margin-top: 10op
  width: 100%;
  label {
    h3 {
      margin-bottom: 10px;
    }
  }
  input {
    width: 100%;
    padding: 10px 0;
    background-color: #fff;
    border-radius: 3px;
    transition: all 0.1s ease-in-out;

    &[type="text"],
    &[type="password"] {
      border: 2px solid rgba(35, 138, 177, 0.25);
      &:focus {
        border-color: rgba(35, 138, 177, 1);
      }
    }

    &[type="submit"] {
      margin-top: 40px;
      border: 2px solid rgba(219, 112, 147, 0.2);
      &:hover {
        background-color: #DB7093;
        border-color: #DB7093;
        color: #fff;
      }
    }
  }
`

class Login extends Component {

  state = {
    username: "",
    password: ""
  }

  handleInputChange = (e) => {
    let name = e.target.name;
    this.setState({ [name]: e.target.value });
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    console.log(this.state);
    console.log(this.props);
    let response = await this.props.login(this.state.username, this.state.password);
    if (response.auth) {
      this.props.history.push('/');
    }
  } 

  componentDidMount = () => {
    if (localStorage.getItem('dloss_auth_token')) {
      this.props.history.goBack();
    }
  }

  render() {
    return (
      <div>
        <h1>Sign in</h1>
        <LoginFormStyle>
          <label>
            <h3>Username</h3>
            <input type="text" name="username" onChange={this.handleInputChange} required />
          </label>
          <label>
            <h3>Password</h3>
            <input type="password" name="password" onChange={this.handleInputChange} required />
          </label>
  
          <input type="submit" value="Submit" onClick={this.handleSubmit} />
        </LoginFormStyle>
      </div>
    );
  }
}

export default Login;