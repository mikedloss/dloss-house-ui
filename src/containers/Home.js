import React, { Component } from 'react';

import HomeCard from '../components/Home/HomeCard';

class Home extends Component {
  render() {
    return (
      <div>
        <HomeCard title="Info 🤔" backgroundColor="#238AB1" route="/info" />
        <HomeCard title="Games 🎲" backgroundColor="#006B98" route="/games" />
        <HomeCard title="Movies 🎥" subtitle="coming soon..." backgroundColor="#D6DBE0" darkText />
      </div>
    );
  }
}

export default Home;