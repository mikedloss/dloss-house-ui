import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const GoBackButtonStyle = styled.button`
  width: 100%;
  border: 2px solid #F7E3FC;
  border-radius: 3px;
  padding: 7px 0;
  margin-bottom: 10px;

  font-weight: 400;
  background-color: #fff;
  color: #000;
  cursor: pointer;

  transition: all 0.1s ease-in-out
  &:hover {
    background-color: palevioletred;
    color: #fff;
  }
`

class GoBackButton extends Component {
  render() {
    return (
      <Link to={this.props.route === undefined ? "/" : this.props.route}>
        <GoBackButtonStyle>
          go back
        </GoBackButtonStyle>
      </Link>
    );
  }
}

export default GoBackButton;