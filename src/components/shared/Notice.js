import React, { Component } from 'react';
import styled from 'styled-components';
import { FaClose } from 'react-icons/lib/fa';

const NoticeStyle = styled.div`
  background-color: #EDD268;
  border-radius: 3px;
  color: #004D74;
  padding: 10px 10px;

  transition: display 1s linear;

  h3, p {
    margin: 0;
  }
  
  .close-button {
    float: right;
    cursor: pointer;
  }

  .hide {
    display: none;
  }
  
`

class Notice extends Component {
  state = {
    isActive: false,
    description: "",
    errors: []
  }

  closeNotice = (e) => {
    this.setState({ isActive: false });
  }

  openNotice = (e) => {
    this.setState({ isActive: true });
  }

  componentWillMount = () => {
    if (this.props.errors) {
      this.openNotice();
    }
  }

  render() {
    let errorsList = null;
    if (this.props.errors) {
      errorsList = this.props.errors.map((error, index) => 
        <li key={index}>{error}</li>
      );
    }

    // console.log("yo");

    return (
      <div>
        {this.state.isActive && 
          <NoticeStyle>
            <FaClose className="close-button" onClick={this.closeNotice} />
            <h3>Notice</h3>
            {this.props.description &&
              <p>{this.props.description}</p>
            }
            <ul>
              {errorsList}
            </ul>
          </NoticeStyle>
        }
      </div>
    );
  }
}

export default Notice;