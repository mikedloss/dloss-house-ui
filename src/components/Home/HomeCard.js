import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const HomeCardStyle = styled.div`
  background-color: ${ props => props.backgroundColor ? props.backgroundColor : "#2D3849" };
  border-radius: 3px;
  color: ${ props => props.darkText ? "#000" : "#fff" };
  padding: 10px;
  margin: 5px 0;

  transition: all 0.1s ease-in-out;
  &:hover {
    transform: scale(1.01);
  }

  a { 
    color: #fff;
    &:hover {
      color: #fff;
    }
  }

  .title { text-align: center; }
  .subtitle {
    text-align: center;
    font-style: italic;
    margin-top: 0;
  }
`

class HomeCard extends Component {
  render() {
    return (
      <Link to={this.props.route ? this.props.route : '/'}>
        <HomeCardStyle {...this.props} >
          <h2 className="title">{this.props.title}</h2>
          <p className="subtitle">{this.props.subtitle}</p>
        </HomeCardStyle>
      </Link>
    );
  }
}

export default HomeCard;