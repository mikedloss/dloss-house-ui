import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Header from './containers/Header';
import Home from './containers/Home';
import Login from './containers/Login';
import Register from './containers/Register';
import Info from './containers/Info';
import Games from './containers/Games';
import GameProfile from './containers/GameProfile';
import Movies from './containers/Movies';
// import Example from './Example/Example';

import { getUser, logoutUser, registerUser, loginUser } from './API';

import './index.css';
// import registerServiceWorker from './registerServiceWorker';

const initialState = {
  username: "",
  isLoggedIn: false,
  isAdmin: false,
  isSpecial: false
}

class App extends Component {

  state = initialState;

  handleLogin = async (username, password) => {
    let response = await loginUser(username, password);
    if (response.auth) {
      localStorage.setItem('dloss_auth_token', response.token);
      await this._getUserInfo(response.token);
      this.setState({
        isLoggedIn: true
      });
    }
    return response;
  }

  handleRegister = async (username, password) => {
    let response = await registerUser(username, password);
    if (response.auth) {
      localStorage.setItem('dloss_auth_token', response.token);
      await this._getUserInfo(response.token);
      this.setState({
        isLoggedIn: true
      });
    }
    return response;
  }

  handleLogout = async () => {
    let response = await logoutUser();
    localStorage.removeItem('dloss_auth_token', "");
    this.setState(initialState);
    return response;
  }

  _getUserInfo = async (token) => {
    let response = await getUser(token);
    if (response) {
      this.setState({
        isSpecial: response.isSpecial,
        isAdmin: response.isAdmin,
        username: response.username
      });
    }
    return response;
  }

  _resetState = () => this.setState(initialState);

  componentDidMount = async () => {
    // determine if we're logged in
    if (localStorage.getItem('dloss_auth_token')) {
      // get username from db
      let response = await this._getUserInfo(localStorage.getItem('dloss_auth_token'));
      if (response.hasOwnProperty('username')) {
        this.setState({
          username: response.username,
          isLoggedIn: true
        })
      }
    }
  }

  render() {
    return (
      <div className="container">
        <Header accountInfo={this.state}
          logout={this.handleLogout} />
        <button onClick={this.handleLogout}>clear storage, logout</button>
        <Switch>
          <Route exact path='/' render={(props) => <Home {...props} />} />
          <Route path='/login' render={(props) => 
            <Login {...props} login={this.handleLogin} />
          }/>
          <Route path='/register' render={(props) => 
            <Register {...props} register={this.handleRegister} />
          }/>
          <Route path='/info' render={(props) => <Info {...props} />} />
          <Route path='/games/:id' render={(props) => <GameProfile {...props} />} />
          <Route exact path='/games' render={(props) => 
            <Games {...props} userInfo={this.state} />
          }/>
          <Route path='/movies' render={(props) => <Movies {...props} />} />
        </Switch>
      </div>
    );
  }
}

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>, 
  document.getElementById('root')
);
// registerServiceWorker();
