import React, { Component } from 'react';
import GoBackButton from '../shared/GoBackButton';

class Example extends Component {
  render() {
    return (
      <div>
        <GoBackButton />
        <h1>The quick onyx goblin jumps over the lazy dwarf</h1>
        <h2>The quick onyx goblin jumps over the lazy dwarf</h2>
        <h3>The quick onyx goblin jumps over the lazy dwarf</h3>
        <h4>The quick onyx goblin jumps over the lazy dwarf</h4>
        <h5>The quick onyx goblin jumps over the lazy dwarf</h5>
        <h6>The quick onyx goblin jumps over the lazy dwarf</h6>
      </div>
    );
  }
}

export default Example;