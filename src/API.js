const API = {
  auth_url: "http://localhost:1234/api/auth",
  games_url: "http://localhost:1234/api/games"
}

export const getUser = async (token) => {
  let url = `${API.auth_url}/me`;

  let response = await fetch(url, {
    method: 'GET',
    headers: {
      'x-access-token': token
    }
  })
  .then(res => res.json());

  // console.log(response.status);

  return response;
}

export const registerUser = async (username, password) => {
  let url = `${API.auth_url}/register`;

  let payload = {
    username, password
  };

  let formBody = Object
                  .keys(payload)
                  .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(payload[key])}`)
                  .join('&');

  let response = await fetch(url, {
    method: 'POST',
    headers: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    body: formBody
  })
  .then(res => res.json());

  return response;
}

export const loginUser = async (username, password) => {
  let url = `${API.auth_url}/login`;

  let payload = {
    username, password
  };

  let formBody = encodeBody(payload)

  let response = await fetch(url, {
    method: 'POST',
    headers: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    body: formBody
  })
  .then(res => {
    if (res.status === 200) {
      return res.json().then(data => {
        return {
          ...data, 
          status: 200
        }
      });
    } else {
      return {
        status: res.status,
        response: res
      };
    }
  });

  return response;
}

export const logoutUser = () => {
  let url = `${API.auth_url}/logout`;

  let response = fetch(url, {
    method: 'GET'
  })
  .then(res => res.json());

  return response;
}

export const getAllGames = () => {
  let url = `${API.games_url}/all`;

  let response = fetch(url,{
    method: 'GET'
  })
  .then(res => res.json());

  return response;
}

export const getGame = (gameId) => {
  let url = `${API.games_url}/${gameId}`;

  let response = fetch(url, {
    method: 'GET'
  })
  .then(res => res.json());

  return response;
}

const encodeBody = (payload) => {
  return Object
          .keys(payload)
          .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(payload[key])}`)
          .join('&');
}